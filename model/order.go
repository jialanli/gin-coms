package model

type Order struct {
	Model
	//ID string `json:"id" gorm:"AUTO_INCREMENT"`
	Name         string       `json:"name"`
	OrderPrice   float64      `json:"price"`                 //订单总额
	CustomerID   string       `json:"customerId" gorm:"not null"` //订单所有者
	CustomerName string       `json:"customerName"`
	Status       string       `json:"status" gorm:"not null"` //订单状态
	OrderItems   *[]OrderItem `json:"orderItems" gorm:"-"`    //订单项列表
}

//订单明细
type OrderItem struct {
	Model
	OrderID  string  `json:"orderId"`
	ShopType string  `json:"shopType"`
	Name     string  `json:"name" gorm:"not null"`
	Price    float64 `json:"price" gorm:"not null"` //单价
	Count    int     `json:"count"`                 //数量
}
