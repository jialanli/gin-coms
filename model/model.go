package model

import "time"

type Model struct {
	ID        string `json:"id" gorm:"primary_key";"AUTO_INCREMENT"`
	CreatedAt int64
	UpdatedAt *time.Time `json:"UpdatedAt,omitempty"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

type ResultList struct {
	Total  int         `json:"total"`
	Result interface{} `json:"result"`
}

type ReturnStruct struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
