package config

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
)

var gdb *gorm.DB

func Connection() *gorm.DB {
	return gdb
}

// 创建数据库链接
func InitMysql() {
	var err error
	if gdb, err = gorm.Open("mysql", MySQLConnection); err != nil {
		log.Fatal(err)
	}

	gdb.DB().SetMaxIdleConns(MySQLMaxIdleConns)
	gdb.DB().SetMaxOpenConns(MySQLMaxOpenConns)
	gdb.LogMode(MySQLLogMode)
	gdb.SingularTable(true)

}
