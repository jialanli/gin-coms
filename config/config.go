package config


var (
	Version  = "1.0.0"
	Name     = "gin-coms"
	ComsAddress = "0.0.0.0"
	ComsPort = "8000"
	MySQLConnection = "root:wl@(127.0.0.1:3308)/gotest?charset=utf8&parseTime=True&loc=Local"
	MySQLLogMode = false
	MySQLMaxOpenConns = 2
	MySQLMaxIdleConns = 1
)

