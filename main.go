package main

import (
	"fmt"
	"gin-coms/config"
	"gin-coms/routers"
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	// 第一组路由：以/api开头
	apiGroup := router.Group("/api")
	{
		routers.CusRoute(apiGroup)
		routers.OrderRoute(apiGroup)
	}

	// 第二组路由：以/other开头
	otherGroup := router.Group("/other")
	{
		routers.OneRoute(otherGroup)
		otherGroup.Use(CheckByToken())	//可以放多个中间件，如router.Use(CheckByToken(),CheckBySession())
		routers.OtherRoute(otherGroup)
	}

	//config.InitMysql()
	router.Run(config.ComsAddress + ":" + config.ComsPort)
}

func CheckByToken() gin.HandlerFunc{
	return func(c *gin.Context){
		fmt.Println("---进入CheckByToken函数---")
		if c.Request.Header.Get("token") == "token-abcde"{
			fmt.Println("验证token成功！")
			// 验证通过时继续往下走访问下一个中间件
			c.Next()
		} else {
			// 验证不通过时请求终止
			c.Abort()
			c.JSON(http.StatusUnauthorized,"验证失败！")
		}
	}
}

func CheckBySession() gin.HandlerFunc{
	return func(c *gin.Context){

	}
}
