package routers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func OtherRoute(group *gin.RouterGroup){
	group.GET("/test", OtherList)
}

func OneRoute(group *gin.RouterGroup){
	group.GET("/one", oneList)
}

func oneList(c *gin.Context){
	fmt.Println("成功进入oneList接口")
	c.String(http.StatusOK, "hello, 这里是/other/one路由")
}

func OtherList(c *gin.Context){
	fmt.Println("成功进入OtherList接口")
	c.String(http.StatusOK, "hello, 这里是/other/test路由")
}
