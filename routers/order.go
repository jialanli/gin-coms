package routers

import (
	"fmt"
	"gin-coms/model"
	"github.com/gin-gonic/gin"
	"net/http"
)

func OrderRoute(group *gin.RouterGroup) {
	group.GET("/order",CheckOrder, OrderList)
	group.GET("/order/:ID", OrderDetail)
	group.GET("/orderByName", OrderByName) // URL的问号会带参数name，http://localhost:8000/api/orderByName?name=%E6%97%A0
}

func CheckOrder(c *gin.Context){
	fmt.Println("---进入check函数---")
	if c.Request.Header.Get("token") == "enyduuamlm2cdcs=="{
		fmt.Println("验证通过！")
		return
	}
	c.Abort()
	c.JSON(http.StatusUnauthorized,"验证失败！")
}

func OrderByName(c *gin.Context) {
	name := c.Query("name") //获取URL路径参数
	fmt.Println("您要查询的订单名称是:", name)
	c.JSON(http.StatusOK, "hello 这里是/api/orderByName路由,您要查询的订单名称是:"+name)
	//c.String(http.StatusOK, "hello 这里是/api/order路由")
}

func OrderDetail(c *gin.Context) {
	id := c.Param("ID") //获取URL路径参数
	fmt.Println("您要查询的是", id, "号订单。")
	c.JSON(http.StatusOK, "hello 这里是/api/order/:ID路由,您要查询的是"+id+"号订单。")
	//c.String(http.StatusOK, "hello 这里是/api/order路由")
}

func OrderList(c *gin.Context) {
	fmt.Println("---验证通过，进入OrderList函数---")
	var orderList []model.Order
	order1 := model.Order{Name: "订单1", OrderPrice: 88}
	order2 := model.Order{Name: "订单2", OrderPrice: 99}
	orderList = append(orderList, order1, order2)
	c.JSON(http.StatusOK, orderList)

	// 如果要返回自定义结构体类型ReturnStruct，需要改源码，在type HandlerFunc func(*Context)后加返回值ReturnStruct即可。
	//return model.ReturnStruct{Code:http.StatusOK,Message:"hello 这里是/api/order路由,此处返回订单列表!",Data:orderList}
}
