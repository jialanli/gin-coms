package routers

import (
	"fmt"
	"gin-coms/model"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func CusRoute(group *gin.RouterGroup) {
	group.POST("/cus", CreateCustomer)
	group.PUT("/cus", UpdateCustomer)
}

func CreateCustomer(c *gin.Context) {
	var cus model.Customer
	err := c.BindJSON(&cus) // 获取请求体参数，请求头为application/json
	if err != nil {
		log.Println(err.Error())
	}
	fmt.Println("用户", cus, "已创建。")
	c.JSON(http.StatusOK, "用户已创建。")
}

func UpdateCustomer(c *gin.Context) {
	name := c.PostForm("name")	// 获取请求体参数，请求头为application/x-www-form-urlencoded
	gender := c.PostForm("gender")
	fmt.Println("用户", name,"  ",gender, "已修改。")
	c.JSON(http.StatusOK, "用户已修改。")
}
